import MySQLdb as db

#HOST = "au-mgt-1-db-01-mysql-5-7.cqvdxooagoob.ap-southeast-2.rds.amazonaws.com"
HOST = "localhost"
PORT = 13307
USER = "au_mgt_db_user"
PASSWORD = "B)U6YhytAZ"
DB = "sonarqube"

try:
    connection = db.Connection(host=HOST, port=PORT,
                               user=USER, passwd=PASSWORD, db=DB)

    dbhandler = connection.cursor()
    dbhandler.execute("SHOW DATABASES")
    result = dbhandler.fetchall()
    for item in result:
        print item[0]

except Exception as e:
    print e

finally:
    connection.close()
